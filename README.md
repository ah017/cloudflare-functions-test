# Test repo for Cloudflare functions routing

https://cloudflare-functions-test.pages.dev

The docs state:

> More specific routes (that is, those with fewer wildcards) take precedence over less specific routes

See: [Functions routing](https://developers.cloudflare.com/pages/platform/functions#functions-routing) 

I would expect that the following routes would be created:  

```
/api/music
/api/music/*
/api/music/*/**
/api/time
```

But what's actually reported on the deployment dashboard is:

```
{
  "routes": [
    {
      "routePath": "/api/music/:id",
      "mountPath": "/api/music",
      "method": "",
      "module": [
        "api/music/[id].js:onRequest"
      ]
    },
    {
      "routePath": "/api/music/:path*",
      "mountPath": "/api/music",
      "method": "",
      "module": [
        "api/music/[[path]].js:onRequest"
      ]
    },
    {
      "routePath": "/api/music",
      "mountPath": "/api/music",
      "method": "",
      "module": [
        "api/music/index.js:onRequest"
      ]
    },
    {
      "routePath": "/api/time",
      "mountPath": "/api",
      "method": "",
      "module": [
        "api/time.js:onRequest"
      ]
    }
  ],
  "baseURL": "/"
}
```
